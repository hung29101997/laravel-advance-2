<?php

use Illuminate\Database\Seeder;

use App\Models as Database;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        $users = [
            ['a@gmail.com', '123456'],
            ['b@gmail.com', '123456'],
            ['c@gmail.com', '123456'],
            ['d@gmail.com', '123456'],
            ['e@gmail.com', '123456'],
            ['f@gmail.com', '123456'],
        ];

        $roles = Database\Role::all();

        foreach ($users as $user) {
            factory(App\Models\User::class)->create([
                'email' => $user[0],
                'password' => $user[1],
                'role_id' => $roles->random()->id
            ]);
        }
    }
}
